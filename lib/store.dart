import 'package:mixlog_db/obj/combo_obj.dart';
import 'package:mixlog_db/obj/tune_obj.dart';
import 'package:mixlog_db/obj/user_obj.dart';

class Store {

  static Store instance = Store._();
  Store._();

  bool initiated = false;
  String userId = 'SAMPLE';
  UserObj userObj = UserObj.empty();
  List<TuneObj> tunes = [];
  List<ComboObj> combos = [];

  int totalReadsCount = 0;

}