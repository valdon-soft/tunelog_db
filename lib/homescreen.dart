import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'obj/tune_obj.dart';
import 'store.dart';
import 'db.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);
  @override State<HomeScreen> createState() => _State();
}

class _State extends State<HomeScreen> {

  late Future<bool> _initFuture;
  final Store _store = Store.instance;
  final FirebaseAuth _auth = FirebaseAuth.instance;

  @override
  void initState() {
    _initFuture = Future(() async {
      if(!_store.initiated){
        await _auth.signInAnonymously();
        _store.userObj = await Db.getUser(_store.userId);
        await Db.getMyTunes();
        await Db.getMyCombos();
      }
      return true;
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Mixlog DB')),
      body: FutureBuilder<Object>(
        future: _initFuture,
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if(snapshot.hasData && snapshot.data){
            return ListView(
              padding: const EdgeInsets.all(24),
              children: [
                const Text('Local tune search examples', textAlign: TextAlign.center),
                const SizedBox(height: 16),
                FilledButton(
                  onPressed: () => Db.searchLocalTunes('Mi'),
                  child: const Text('"Mi"'),
                ),
                const SizedBox(height: 8),
                FilledButton(
                  onPressed: () => Db.searchLocalTunes('Mic'),
                  child: const Text('"Mic"'),
                ),
                const SizedBox(height: 8),
                FilledButton(
                  onPressed: () => Db.searchLocalTunes('Mic Jac'),
                  child: const Text('"Mic Jac"'),
                ),
                const SizedBox(height: 8),
                FilledButton(
                  onPressed: () => Db.searchLocalTunes('Michael Jackson'),
                  child: const Text('"Michael Jackson"'),
                ),
                const SizedBox(height: 8),
                FilledButton(
                  onPressed: () => Db.searchLocalTunes('Bil'),
                  child: const Text('"Bil"'),
                ),
                const SizedBox(height: 8),
                FilledButton(
                  onPressed: () => Db.searchLocalTunes('Jea'),
                  child: const Text('"Jea"'),
                ),
                const SizedBox(height: 8),
                FilledButton(
                  onPressed: () => Db.searchLocalTunes('Billie'),
                  child: const Text('"Billie"'),
                ),
                const SizedBox(height: 8),
                FilledButton(
                  onPressed: () => Db.searchLocalTunes('Mic Bil'),
                  child: const Text('"Mic Bil"'),
                ),
                const SizedBox(height: 16),
                const Text('Firestore tune search examples', textAlign: TextAlign.center),
                const SizedBox(height: 16),
                FilledButton(
                  onPressed: () => Db.searchFirestoreTunes('Mi'),
                  child: const Text('"Mi"'),
                ),
                const SizedBox(height: 8),
                FilledButton(
                  onPressed: () => Db.searchFirestoreTunes('Mic'),
                  child: const Text('"Mic"'),
                ),
                const SizedBox(height: 8),
                FilledButton(
                  onPressed: () => Db.searchFirestoreTunes('Michael'),
                  child: const Text('"Michael"'),
                ),
                const SizedBox(height: 8),
                FilledButton(
                  onPressed: () => Db.searchFirestoreTunes('Jean'),
                  child: const Text('"Jean"'),
                ),
                const SizedBox(height: 16),
                const Text('Combos', textAlign: TextAlign.center),
                const SizedBox(height: 16),
                FilledButton(
                  onPressed: (){
                    int tuneIndex = _store.tunes.indexWhere((e) => e.name == 'Billie Jean');
                    if(tuneIndex > -1){
                      Db.getLocalComboSuggestion(_store.tunes[tuneIndex].tunelogId);
                    } else {
                      print('Could not find a tune "Billie Jean"');
                    }
                  },
                  child: const Text('Local suggestion for "Billie Jean"'),
                ),
                const SizedBox(height: 8),
                FilledButton(
                  onPressed: (){
                    int tuneIndex = _store.tunes.indexWhere((e) => e.name == 'Billie Jean');
                    if(tuneIndex > -1){
                      Db.getFirestoreComboSuggestion(_store.tunes[tuneIndex].tunelogId);
                    } else {
                      print('Could not find a tune "Billie Jean"');
                    }
                  },
                  child: const Text('Firestore suggestion for "Billie Jean"'),
                ),
              ],
            );
          } else {
            return Container();
          }
        }
      ),
    );
  }
}
