
class MyTuneFields {
  static const uid = 'uid';
  static const added = 'added';
  static const cd = 'cd';
  static const digital = 'digital';
  static const source = 'source';
  static const tape = 'tape';
  static const tunelog = 'tunelog';
  static const updated = 'updated';
  static const user = 'user';
  static const vinyl = 'vinyl';
}

class MyTuneObj {

  final String uid;
  final int added;
  final bool cd;
  final bool digital;
  final String source;
  final bool tape;
  final String tunelog;
  final String user;
  final bool vinyl;

  MyTuneObj({
    required this.uid,
    required this.added,
    required this.cd,
    required this.digital,
    required this.source,
    required this.tape,
    required this.tunelog,
    required this.user,
    required this.vinyl,
  });

  factory MyTuneObj.fromMap(String uid, Map<String, dynamic> map) => MyTuneObj(
    uid: uid,
    added: map[MyTuneFields.added] ?? 0,
    cd: map[MyTuneFields.cd] ?? false,
    digital: map[MyTuneFields.digital] ?? false,
    source: map[MyTuneFields.source] ?? '',
    tape: map[MyTuneFields.tape] ?? false,
    tunelog: map[MyTuneFields.tunelog] ?? '',
    user: map[MyTuneFields.user] ?? '',
    vinyl: map[MyTuneFields.vinyl] ?? false,
  );

  factory MyTuneObj.fromTunelog(String tunelogId) => MyTuneObj(
    uid: '',
    added: 0,
    cd: false,
    digital: false,
    source: '',
    tape: false,
    tunelog: tunelogId,
    user: '',
    vinyl: false,
  );

}
