import 'my_tune_obj.dart';
import 'tunelog_obj.dart';

class TuneObj {

  // From MyTuneObj
  final String uid;
  final int added;
  final bool cd;
  final bool digital;
  final String source;
  final bool tape;
  final String tunelog;
  final String user;
  final bool vinyl;
  // From TunelogObj
  final String tunelogId;
  final String appleId;
  final String artist;
  final String artistStrip;
  final List<String> artistTags;
  final double bpm;
  final String fullStrip;
  final String key;
  final String name;
  final String nameStrip;
  final List<String> nameTags;
  final int releaseDate;
  final String spotifyId;
  final bool verified;

  TuneObj({
    // From MyTuneObj
    required this.uid,
    required this.added,
    required this.cd,
    required this.digital,
    required this.source,
    required this.tape,
    required this.tunelog,
    required this.user,
    required this.vinyl,
    // From TunelogObj
    required this.tunelogId,
    required this.appleId,
    required this.artist,
    required this.artistStrip,
    required this.artistTags,
    required this.bpm,
    required this.fullStrip,
    required this.key,
    required this.name,
    required this.nameStrip,
    required this.nameTags,
    required this.releaseDate,
    required this.spotifyId,
    required this.verified,
  });

  factory TuneObj.fromObj(MyTuneObj myTuneObj, TunelogObj tunelogObj) => TuneObj(
    // From MyTuneObj
    uid: myTuneObj.uid,
    added: myTuneObj.added,
    cd: myTuneObj.cd,
    digital: myTuneObj.digital,
    source: myTuneObj.source,
    tape: myTuneObj.tape,
    tunelog: myTuneObj.tunelog,
    user: myTuneObj.user,
    vinyl: myTuneObj.vinyl,
    // From TunelogObj
    tunelogId: tunelogObj.uid,
    appleId: tunelogObj.appleId,
    artist: tunelogObj.artist,
    artistStrip: tunelogObj.artistStrip,
    artistTags: tunelogObj.artistTags,
    bpm: tunelogObj.bpm,
    fullStrip: tunelogObj.fullStrip,
    key: tunelogObj.key,
    name: tunelogObj.name,
    nameStrip: tunelogObj.nameStrip,
    nameTags: tunelogObj.nameTags,
    releaseDate: tunelogObj.releaseDate,
    spotifyId: tunelogObj.spotifyId,
    verified: tunelogObj.verified,
  );

}
