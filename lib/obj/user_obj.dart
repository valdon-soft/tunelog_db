
class UserFields {
  static const uid = 'uid';
  static const appVersion = 'appVersion';
  static const combosUpdate = 'combosUpdate';
  static const device = 'device';
  static const deviceVersion = 'deviceVersion';
  static const email = 'email';
  static const name = 'name';
  static const photo = 'photo';
  static const realKeys = 'realKeys';
  static const token = 'token';
  static const tunesUpdate = 'tunesUpdate';
}

class UserObj {

  final String uid;
  final int appVersion; // use only the build number, to keep it an integer. Example: 45 (from 1.0.1+45)
  final int combosUpdate; // last local storage update (milliseconds since epoch)
  final String device; // iPhone, etc
  final String deviceVersion; // '1.6', etc
  final String email;
  final String name;
  final String photo; // id of a photo in FB Storage
  final bool realKeys; // user setting - show real keys (F#m) or the translated (11A)
  final String token; // device token for notifications
  final int tunesUpdate; // last local storage update (milliseconds since epoch)

  UserObj({
    required this.uid,
    required this.appVersion,
    required this.combosUpdate,
    required this.device,
    required this.deviceVersion,
    required this.email,
    required this.name,
    required this.photo,
    required this.realKeys,
    required this.token,
    required this.tunesUpdate,
  });

  factory UserObj.fromMap(String uid, Map<String, dynamic> map) => UserObj(
    uid: uid,
    appVersion: map[UserFields.appVersion] ?? 0,
    combosUpdate: map[UserFields.combosUpdate] ?? 0,
    device: map[UserFields.device] ?? '',
    deviceVersion: map[UserFields.deviceVersion] ?? '',
    email: map[UserFields.email] ?? '',
    name: map[UserFields.name] ?? '',
    photo: map[UserFields.photo] ?? '',
    realKeys: map[UserFields.realKeys] ?? false,
    token: map[UserFields.token] ?? '',
    tunesUpdate: map[UserFields.tunesUpdate] ?? 0,
  );

  factory UserObj.empty() => UserObj(
    uid: '',
    appVersion: 0,
    combosUpdate: 0,
    device: '',
    deviceVersion: '',
    email: '',
    name: '',
    photo: '',
    realKeys: false,
    token: '',
    tunesUpdate: 0,
  );

}
