import 'tune_obj.dart';

class TunelogFields {
  static const uid = 'uid';
  static const appleId = 'appleId';
  static const artist = 'artist';
  static const artistStrip = 'artistStrip';
  static const artistTags = 'artistTags';
  static const bpm = 'bpm';
  static const fullStrip = 'fullStrip';
  static const key = 'key';
  static const name = 'name';
  static const nameStrip = 'nameStrip';
  static const nameTags = 'nameTags';
  static const releaseDate = 'releaseDate';
  static const spotifyId = 'spotifyId';
  static const verified = 'verified';
}

class TunelogObj {

  final String uid;
  final String appleId;
  final String artist;
  final String artistStrip;
  final List<String> artistTags;
  final double bpm;
  final String fullStrip;
  final String key;
  final String name;
  final String nameStrip;
  final List<String> nameTags;
  final int releaseDate;
  final String spotifyId;
  final bool verified;

  TunelogObj({
    required this.uid,
    required this.appleId,
    required this.artist,
    required this.artistStrip,
    required this.artistTags,
    required this.bpm,
    required this.key,
    required this.fullStrip,
    required this.name,
    required this.nameStrip,
    required this.nameTags,
    required this.releaseDate,
    required this.spotifyId,
    required this.verified,
  });

  factory TunelogObj.fromMap(String uid, Map<String, dynamic> map) => TunelogObj(
    uid: uid,
    appleId: map[TunelogFields.appleId] ?? '',
    artist: map[TunelogFields.artist] ?? '',
    artistStrip: map[TunelogFields.artistStrip] ?? '',
    artistTags: map[TunelogFields.artistTags].cast<String>(),
    bpm: map[TunelogFields.bpm] != null ? double.parse(map[TunelogFields.bpm].toString()) : -1,
    fullStrip: map[TunelogFields.fullStrip] ?? '',
    key: map[TunelogFields.key] ?? '',
    name: map[TunelogFields.name] ?? '',
    nameStrip: map[TunelogFields.nameStrip] ?? '',
    nameTags: map[TunelogFields.nameTags].cast<String>(),
    releaseDate: map[TunelogFields.releaseDate] ?? '',
    spotifyId: map[TunelogFields.spotifyId] ?? '',
    verified: map[TunelogFields.verified] ?? false,
  );

  factory TunelogObj.fromTuneObj(TuneObj tuneObj) => TunelogObj(
    uid: tuneObj.tunelog,
    appleId: tuneObj.appleId,
    artist: tuneObj.artist,
    artistStrip: tuneObj.artistStrip,
    artistTags: List.from(tuneObj.artistTags),
    bpm: tuneObj.bpm,
    fullStrip: tuneObj.fullStrip,
    key: tuneObj.key,
    name: tuneObj.name,
    nameStrip: tuneObj.nameStrip,
    nameTags: List.from(tuneObj.nameTags),
    releaseDate: tuneObj.releaseDate,
    spotifyId: tuneObj.spotifyId,
    verified: tuneObj.verified,
  );

}
