
class ComboFields {
  static const uid = 'uid';
  static const created = 'created';
  static const public = 'public';
  static const reversible = 'reversible';
  static const tag = 'tag';
  static const tune1 = 'tune1';
  static const tune2 = 'tune2';
  static const updated = 'updated';
  static const user = 'user';
}

class ComboObj {

  final String uid;
  final int created;
  final bool public;
  final bool reversible;
  final int tag;
  final String tune1;
  final String tune2;
  final int updated;
  final String user;

  ComboObj({
    required this.uid,
    required this.created,
    required this.public,
    required this.reversible,
    required this.tag,
    required this.tune1,
    required this.tune2,
    required this.updated,
    required this.user,
  });

  factory ComboObj.fromMap(String uid, Map<String, dynamic> map) => ComboObj(
    uid: uid,
    created: map[ComboFields.created] ?? 0,
    public: map[ComboFields.public] ?? false,
    reversible: map[ComboFields.reversible] ?? false,
    tag: map[ComboFields.tag] ?? 0,
    tune1: map[ComboFields.tune1] ?? '',
    tune2: map[ComboFields.tune2] ?? '',
    updated: map[ComboFields.updated] ?? 0,
    user: map[ComboFields.user] ?? '',
  );

}
