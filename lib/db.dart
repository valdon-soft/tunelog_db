import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:mixlog_db/obj/combo_obj.dart';
import 'package:mixlog_db/store.dart';
import 'package:mixlog_db/obj/my_tune_obj.dart';
import 'package:mixlog_db/obj/tunelog_obj.dart';
import 'package:mixlog_db/obj/tune_obj.dart';
import 'package:mixlog_db/obj/user_obj.dart';

class Db {

  static final Store _store = Store.instance;
  static final FirebaseFirestore _fb = FirebaseFirestore.instance;
  static final CollectionReference _users = _fb.collection('users2');
  static final CollectionReference _tunes = _fb.collection('tunes2');
  static final CollectionReference _tunelog = _fb.collection('tunelog');
  static final CollectionReference _combos = _fb.collection('combos');
  static const int _batchChunkSize = 500; //max allowed by Firebase

  static Future<UserObj> getUser(String uid) async {
    DocumentSnapshot doc = await _users.doc(uid).get();
    Map<String, dynamic> map = doc.data() as Map<String, dynamic>;
    return UserObj.fromMap(doc.id, map);
  }

  static Future<List<TuneObj>> getMyTunes() async {
    final List<TuneObj> tunes = [];
    // First check the local storage
    // The local storage version is saved to the user's profile ('tunesUpdate') with a timestamp (milliseconds since epoch)
    // We should get data from Firestore only if the user login from another device
    // TODO: Get actual local storage VERSION from the local storage or from Shared Preferences / Secure Storage
    const int tunesLocalStorageVersion = 1234; // example, because no local storage on this project
    if(_store.userObj.tunesUpdate != tunesLocalStorageVersion){
      print('GET TUNES FROM FIRESTORE');
      // Get only the tunes, which were updated after my local storage date
      try {
        final QuerySnapshot myTunesSnapshot = await _tunes
          .where(MyTuneFields.user, isEqualTo: _store.userObj.uid)
          .where(MyTuneFields.updated, isGreaterThan: tunesLocalStorageVersion)
          .get();
        if(myTunesSnapshot.docs.isNotEmpty){
          _store.totalReadsCount += myTunesSnapshot.docs.length;
          for(int i = 0; i < myTunesSnapshot.docs.length; i++){
            final String myTuneId = myTunesSnapshot.docs[i].id;
            final Map<String, dynamic> myTuneMap = myTunesSnapshot.docs[i].data() as Map<String, dynamic>;
            final MyTuneObj myTuneObj = MyTuneObj.fromMap(myTuneId, myTuneMap);
            final String tunelogId = myTuneObj.tunelog;
            // Search tunelog item in local storage first
            bool localTunelogFound = false;
            for(int j = 0; j < _store.tunes.length; j++){
              if(_store.tunes[j].tunelogId == tunelogId){
                localTunelogFound = true;
                final TunelogObj tunelogObj = TunelogObj.fromTuneObj(_store.tunes[j]);
                final TuneObj tuneObj = TuneObj.fromObj(myTuneObj, tunelogObj);
                tunes.add(tuneObj);
                break;
              }
            }
            if(!localTunelogFound){
              _store.totalReadsCount++;
              final DocumentSnapshot doc = await _tunelog.doc(tunelogId).get();
              final Map<String, dynamic> tunelogMap = doc.data() as Map<String, dynamic>;
              final TunelogObj tunelogObj = TunelogObj.fromMap(tunelogId, tunelogMap);
              final TuneObj tuneObj = TuneObj.fromObj(myTuneObj, tunelogObj);
              tunes.add(tuneObj);
            }
          }
        }
      } catch (e) {
        print(e.toString());
      }
    } else {
      print('GET TUNES FROM LOCAL STORAGE');
      // TODO: Get the list from the local storage instead
    }
    // Debug:
    print('${tunes.length} tunes');
    for(int i = 0; i < tunes.length; i++){
      print('tune $i artist: ${tunes[i].artist}');
      print('tune $i name: ${tunes[i].name}');
      print('tune $i ID: ${tunes[i].uid}');
    }
    _store.tunes.clear();
    _store.tunes.addAll(tunes);
    return tunes;
  }

  static Future<List<TuneObj>> searchLocalTunes(String searchStr) async {
    // Always limit the search results (3, for example)
    const maxCount = 3;
    final List<TuneObj> tunes = [];
    final List<String> tuneIds = [];
    //
    bool haveOneWordWithMoreThan2Chars = false;
    final String str = searchStr.toLowerCase();
    final List<String> words = str.split(' ');
    final List<String> wordsStrip = [];
    for(int i = 0; i < words.length; i++){
      final String wordStrip = words[i].replaceAll(' ', '').replaceAll(RegExp(r'[^\w\s]+'),'');
      if(wordStrip.length > 2){
        haveOneWordWithMoreThan2Chars = true;
      }
      wordsStrip.add(wordStrip);
    }
    final String fullStrip = wordsStrip.join('');
    //
    // The search string must contain at least one word with more than 2 characters
    if(haveOneWordWithMoreThan2Chars){
      // TODO: Use actual local storage, not the temp storage used here
      // int count = 0;
      // Priority 1. Full match
      if(tunes.length < maxCount){
        for(int i = 0; i < _store.tunes.length; i++){
          if(!tuneIds.contains(_store.tunes[i].uid)) {
            bool add = false;
            if(_store.tunes[i].fullStrip == fullStrip){
              add = true;
            }
            if(_store.tunes[i].artistStrip == fullStrip){
              add = true;
            }
            if(_store.tunes[i].nameStrip == fullStrip){
              add = true;
            }
            if(add){
              tunes.add(_store.tunes[i]);
              tuneIds.add(_store.tunes[i].uid);
              if(tunes.length >= maxCount){
                break;
              }
            }
          }
        }
      }
      // Priority 2. Check for full word match
      if(tunes.length < maxCount){
        for(int i = 0; i < wordsStrip.length; i++){
          if(tunes.length < maxCount){
            for(int j = 0; j < _store.tunes.length; j++){
              if(!tuneIds.contains(_store.tunes[j].uid)){
                bool add = false;
                if(_store.tunes[j].artistTags.contains(wordsStrip[i])){
                  add = true;
                }
                if(_store.tunes[j].nameTags.contains(wordsStrip[i])){
                  add = true;
                }
                if(add){
                  tunes.add(_store.tunes[j]);
                  tuneIds.add(_store.tunes[j].uid);
                  if(tunes.length >= maxCount){
                    break;
                  }
                }
              }
            }
          }
        }
      }
      // Priority 3. Check for artist or name starts with
      if(tunes.length < maxCount){
        for(int i = 0; i < wordsStrip.length; i++){
          if(tunes.length < maxCount){
            for(int j = 0; j < _store.tunes.length; j++){
              if(!tuneIds.contains(_store.tunes[j].uid)){
                bool add = false;
                final String artist = _store.tunes[j].artist.toLowerCase();
                final String name = _store.tunes[j].name.toLowerCase();
                if(artist.startsWith(wordsStrip[i])){
                  add = true;
                }
                if(name.startsWith(wordsStrip[i])){
                  add = true;
                }
                if(add){
                  tunes.add(_store.tunes[j]);
                  tuneIds.add(_store.tunes[j].uid);
                  if(tunes.length >= maxCount){
                    break;
                  }
                }
              }
            }
          }
        }
      }
      // Priority 4. Check for each word/tag starts with
      if(tunes.length < maxCount){
        for(int i = 0; i < wordsStrip.length; i++){
          if(tunes.length < maxCount){
            for(int j = 0; j < _store.tunes.length; j++){
              if(!tuneIds.contains(_store.tunes[j].uid)){
                bool add = false;
                if(_store.tunes[j].artistTags.length > 1){
                  for(int k = 0; k < _store.tunes[j].artistTags.length; k++){
                    final String tag = _store.tunes[j].artistTags[k].toLowerCase();
                    if(tag.startsWith(wordsStrip[i])){
                      add = true;
                    }
                  }
                }
                if(_store.tunes[j].nameTags.length > 1){
                  for(int k = 0; k < _store.tunes[j].nameTags.length; k++){
                    final String tag = _store.tunes[j].nameTags[k].toLowerCase();
                    if(tag.startsWith(wordsStrip[i])){
                      add = true;
                    }
                  }
                }
                if(add){
                  tunes.add(_store.tunes[j]);
                  tuneIds.add(_store.tunes[j].uid);
                  if(tunes.length >= maxCount){
                    break;
                  }
                }
              }
            }
          }
        }
      }
      // Debug:
      print('Local tune search: ${tunes.length} tune(s) found for "$searchStr": ');
      for(int i = 0; i < tunes.length; i++){
        print('${tunes[i].artist} - ${tunes[i].name}');
      }
    } else {
      print('Search string is too short');
    }
    return tunes;
  }

  static Future<List<TuneObj>> searchFirestoreTunes(String searchStr) async {
    // Always limit the search results (3, for example)
    // Note: Firestore search will not find parts of words
    const maxCount = 3;
    final List<TuneObj> tunes = [];
    final List<String> tuneIds = [];
    //
    bool haveOneWordWithMoreThan2Chars = false;
    final String str = searchStr.toLowerCase();
    final List<String> words = str.split(' ');
    final List<String> wordsStrip = [];
    for(int i = 0; i < words.length; i++){
      final String wordStrip = words[i].replaceAll(' ', '').replaceAll(RegExp(r'[^\w\s]+'),'');
      if(wordStrip.length > 2){
        haveOneWordWithMoreThan2Chars = true;
      }
      wordsStrip.add(wordStrip);
    }
    final String fullStrip = wordsStrip.join('');
    //
    // The search string must contain at least one word with more than 2 characters
    if(haveOneWordWithMoreThan2Chars){
      // Priority 1. Full match
      if(tunes.length < maxCount) {
        final QuerySnapshot query = await _tunelog
          .where(TunelogFields.fullStrip, isEqualTo: fullStrip)
          .limit(maxCount)
          .get();
        if (query.docs.isNotEmpty) {
          _store.totalReadsCount += query.docs.length;
          for (int i = 0; i < query.docs.length; i++) {
            final String tunelogId = query.docs[i].id;
            final Map<String, dynamic> map = query.docs[i].data() as Map<String, dynamic>;
            final TunelogObj tunelogObj = TunelogObj.fromMap(tunelogId, map);
            final TuneObj tuneObj = TuneObj.fromObj(MyTuneObj.fromTunelog(tunelogId), tunelogObj);
            tunes.add(tuneObj);
          }
        }
      }
      // Priority 2. Check for full word match
      if(tunes.length < maxCount){
        final QuerySnapshot query = await _tunelog
          .where(TunelogFields.artistTags, arrayContainsAny: wordsStrip)
          .limit(maxCount - tunes.length)
          .get();
        if(query.docs.isNotEmpty){
          _store.totalReadsCount += query.docs.length;
          for(int i = 0; i < query.docs.length; i++){
            final String tunelogId = query.docs[i].id;
            final Map<String, dynamic> map = query.docs[i].data() as Map<String, dynamic>;
            final TunelogObj tunelogObj = TunelogObj.fromMap(tunelogId, map);
            final TuneObj tuneObj = TuneObj.fromObj(MyTuneObj.fromTunelog(tunelogId), tunelogObj);
            tunes.add(tuneObj);
          }
        }
      }
      if(tunes.length < maxCount){
        final QuerySnapshot query = await _tunelog
          .where(TunelogFields.nameTags, arrayContainsAny: wordsStrip)
          .limit(maxCount - tunes.length)
          .get();
        if(query.docs.isNotEmpty){
          _store.totalReadsCount += query.docs.length;
          for(int i = 0; i < query.docs.length; i++){
            final String tunelogId = query.docs[i].id;
            final Map<String, dynamic> map = query.docs[i].data() as Map<String, dynamic>;
            final TunelogObj tunelogObj = TunelogObj.fromMap(tunelogId, map);
            final TuneObj tuneObj = TuneObj.fromObj(MyTuneObj.fromTunelog(tunelogId), tunelogObj);
            tunes.add(tuneObj);
          }
        }
      }
      // Debug:
      print('Firestore tune search: ${tunes.length} tune(s) found for "$searchStr": ');
      for(int i = 0; i < tunes.length; i++){
        print('${tunes[i].artist} - ${tunes[i].name}');
      }
    } else {
      print('Search string is too short');
    }
    return tunes;
  }

  static Future<List<ComboObj>> getMyCombos() async {
    final List<ComboObj> combos = [];
    // First check the local storage
    // The local storage version is saved to the user's profile ('combosUpdate') with a timestamp (milliseconds since epoch)
    // We should get data from Firestore only if the user login from another device
    // TODO: Get actual local storage VERSION from the local storage or from Shared Preferences / Secure Storage
    const int combosLocalStorageVersion = 1234; // example, because no local storage on this project
    if(_store.userObj.combosUpdate != combosLocalStorageVersion){
      print('GET COMBOS FROM FIRESTORE');
      // Get only the tunes, which were updated after my local storage date
      try {
        final QuerySnapshot snapshot = await _combos
          .where(ComboFields.user, isEqualTo: _store.userObj.uid)
          .where(ComboFields.updated, isGreaterThan: combosLocalStorageVersion)
          .get();
        if(snapshot.docs.isNotEmpty){
          _store.totalReadsCount += snapshot.docs.length;
          for(int i = 0; i < snapshot.docs.length; i++){
            final String comboId = snapshot.docs[i].id;
            final Map<String, dynamic> comboMap = snapshot.docs[i].data() as Map<String, dynamic>;
            final ComboObj comboObj = ComboObj.fromMap(comboId, comboMap);
            combos.add(comboObj);
          }
        }
      } catch (e) {
        print(e.toString());
      }
    } else {
      print('GET COMBOS FROM LOCAL STORAGE');
      // TODO: Get the list from the local storage instead
    }
    // Debug:
    print('${combos.length} combos');
    for(int i = 0; i < combos.length; i++){
      print('combo $i ID: ${combos[i].uid}');
      print('combo $i tune1: ${combos[i].tune1}');
      print('combo $i tune2: ${combos[i].tune2}');
    }
    _store.combos.clear();
    _store.combos.addAll(combos);
    return combos;
  }

  static Future<List<ComboObj>> getLocalComboSuggestion(String tunelogId) async {
    // Always limit the search results (3, for example)
    const maxCount = 3;
    final List<ComboObj> combos = [];
    // TODO use real local storage
    for(int i = 0; i < _store.combos.length; i++){
      bool found = false;
      if(_store.combos[i].tune1 == tunelogId){
        combos.add(_store.combos[i]);
        found = true;
        if(combos.length >= maxCount){
          break;
        }
      }
      if(!found){
        if(_store.combos[i].reversible){
          if(_store.combos[i].tune2 == tunelogId){
            combos.add(_store.combos[i]);
            if(combos.length >= maxCount){
              break;
            }
          }
        }
      }
    }
    //////////////////
    // Debug:
    //////////////////
    final TuneObj tune = _store.tunes[_store.tunes.indexWhere((e) => e.tunelogId == tunelogId)];
    if(combos.isNotEmpty){
      print('Found ${combos.length} combo suggestion(s) for ${tune.name}: ');
      for(int i = 0; i < combos.length; i++){
        String tuneId = combos[i].tune1 == tunelogId ? combos[i].tune2 : combos[i].tune1;
        final int tuneIndex = _store.tunes.indexWhere((e) => e.tunelogId == tuneId);
        if(tuneIndex > -1){
          print('${i+1}. ${_store.tunes[tuneIndex].artist} - ${_store.tunes[tuneIndex].name}');
        } else {
          print('${i+1}. Tune not in local storage ($tuneId)');
        }
      }
    } else {
      print('No combo suggestions found for ${tune.name}');
    }
    return combos;
  }

  static Future<List<ComboObj>> getFirestoreComboSuggestion(String tunelogId) async {
    // Always limit the search results (3, for example)
    const maxCount = 3;
    final List<ComboObj> combos = [];
    try{
      // Priority 1. Match tune1
      if(combos.length < maxCount){
        QuerySnapshot snapshot = await _combos
        // .where(ComboFields.reversible, isEqualTo: true)
          .where(ComboFields.tune1, isEqualTo: tunelogId)
          .limit(maxCount - combos.length)
          .get();
        if(snapshot.docs.isNotEmpty){
          _store.totalReadsCount += snapshot.docs.length;
          for(int i = 0; i < snapshot.docs.length; i++){
            final String comboId = snapshot.docs[i].id;
            final Map<String, dynamic> comboMap = snapshot.docs[i].data() as Map<String, dynamic>;
            final ComboObj comboObj = ComboObj.fromMap(comboId, comboMap);
            combos.add(comboObj);
          }
        }
      }
      // If not enough suggestions...
      // Priority 2. Match tune2 (reversible)
      if(combos.length < maxCount){
        QuerySnapshot snapshot = await _combos
          .where(ComboFields.reversible, isEqualTo: true)
          .where(ComboFields.tune2, isEqualTo: tunelogId)
          .limit(maxCount - combos.length)
          .get();
        if(snapshot.docs.isNotEmpty){
          _store.totalReadsCount += snapshot.docs.length;
          for(int i = 0; i < snapshot.docs.length; i++){
            final String comboId = snapshot.docs[i].id;
            final Map<String, dynamic> comboMap = snapshot.docs[i].data() as Map<String, dynamic>;
            final ComboObj comboObj = ComboObj.fromMap(comboId, comboMap);
            combos.add(comboObj);
          }
        }
      }
    } catch (e) {
      print(e.toString());
    }
    //
    //////////////////
    // Debug:
    //////////////////
    TuneObj tune = _store.tunes[_store.tunes.indexWhere((e) => e.tunelogId == tunelogId)];
    print('Found ${combos.length} combo suggestion(s) for ${tune.name}: ');
    for(int i = 0; i < combos.length; i++){
      for(int i = 0; i < combos.length; i++){
        String tuneId = combos[i].tune1 == tunelogId ? combos[i].tune2 : combos[i].tune1;
        final int tuneIndex = _store.tunes.indexWhere((e) => e.tunelogId == tuneId);
        if(tuneIndex > -1){
          print('${i+1}. ${_store.tunes[tuneIndex].artist} - ${_store.tunes[tuneIndex].name}');
        } else {
          TunelogObj tunelogObj = await getTunelogById(tuneId);
          _store.totalReadsCount++;
          print('${i+1}. ${tunelogObj.artist} - ${tunelogObj.name}');
        }
      }
    }
    return combos;
  }

  static Future<TunelogObj> getTunelogById(String uid) async {
    DocumentSnapshot doc = await _tunelog.doc(uid).get();
    return TunelogObj.fromMap(uid, doc.data() as Map<String, dynamic>);
  }

}
